# Matemática 733 TP1

Momentos
TSP1
TSP2
Fecha de publicación
09/03/2024
27/04/2024
Objetivos a
evaluarFecha de entrega
Primer 50%Varia para cada
asignatura
(Máx 48 hrs)
Segundo 50%Varia para cada
asignatura
(Máx 48 hrs)

Si el trabajo práctico lo realiza usando un procesador de textos (Word, OpenOffice,
LibreOffice). Utilice letra tipo Arial, tamaño 11 o Times New Román, tamaño 12.
Emplee el editor de ecuaciones donde sea requerido. Si el trabajo lo realiza a mano,
escriba con una letra legible y clara. Utilice bolígrafo o marcador punta fina de color
negro. Si el trabajo no es legible el asesor/profesor está en la libertad de devolverlo al
estudiante sin corregirlo.


El envío de los trabajos realizados a su nivel corrector debe realizarse en un solo
archivo. No se recibirán los trabajos enviados a través de imágenes tipo foto en
varios archivos. En caso contrario el asesor/profesor deberá devolverlo al estudiante
sin corregirlo.

